from pathlib import Path

import cv2
import geojson
import matplotlib
import matplotlib.patches as patches
import numpy as np
import pandas as pd
import rawpy
from matplotlib import pyplot as plt

matplotlib.use('TkAgg')

path_images = Path(__file__).parent.resolve()
images = sorted(path_images.rglob("*.CR2"))
aligns = sorted((path_images / "aligns").rglob("*.geojson"))
plot_shapes = sorted((path_images / "plot_shapes").rglob("*.geojson"))
heads = sorted((path_images / "heads").rglob("*.txt"))

for image in images:
    # Read image
    raw = rawpy.imread(str(image))
    rgb = raw.postprocess(user_flip=0)
    # Read aligns
    align = next((geojson.load(open(p)) for p in (path_images / "aligns").rglob(image.stem + "*.geojson")), None)
    # Read plot shapes
    plot_shape = next((geojson.load(open(p)) for p in (path_images / "plot_shapes").rglob(image.stem.split("_")[0] + "*.geojson")), None)
    # Read heads
    head = next((pd.read_table(p, sep=" ", header=None, index_col=0) for p in (path_images / "heads").rglob(image.stem + "*.txt")), None)

    # Create a plot
    fig, axs = plt.subplots(2, figsize=(10, 10))
    # Plot the image
    [ax.imshow(rgb) for ax in axs]

    # Plot the polygon
    if align is not None:
        for polygon_coordinates in align['features'][1]['geometry']['coordinates']:
            [ax.add_patch(patches.Polygon(polygon_coordinates, edgecolor='r', facecolor='none', linewidth=2)) for ax in axs]
            if plot_shape is not None:
                # Affine transform coordinates from 0...3333 0...5000 to align polygon coordinates
                target_coords = np.float32(np.array(polygon_coordinates)[0:4, :])
                orig_coords = np.float32([[0, 0], [0, 5000], [3333, 5000], [3333, 0]])
                # Get perspective transform matrix to transform from 0:1, 0:1 space to image coords
                M_normalized_to_image_coords = cv2.getPerspectiveTransform(
                    orig_coords[0:4, :], target_coords[0:4, :]
                )
                # Transform the row shapes to the image space
                for shape in plot_shape['features']:
                    shape_rows_t = cv2.perspectiveTransform(
                        np.float32([shape['geometry']['coordinates'][0],])[0:4, :], M_normalized_to_image_coords
                    )

                    [ax.add_patch(patches.Polygon(shape_rows_t[0, :], edgecolor='g', facecolor='none', linewidth=2)) for ax in axs]

    # Plot bounding boxes
    if head is not None:
        for _, row in head.iterrows():
            x, y, width, height, conf = row.values
            x -= width / 2
            y -= height / 2
            for ax in axs:
                rect = patches.Rectangle((x * rgb.shape[1], y * rgb.shape[0]), width * rgb.shape[1], height * rgb.shape[0],
                                         edgecolor='b', facecolor='none', linewidth=1)
                ax.add_patch(rect)

    # Show the plot
    axs[1].set_xlim(2000, 2500)
    axs[1].set_ylim(2000, 2500)
    plt.show()


