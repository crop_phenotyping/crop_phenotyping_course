import torch
from yolov5.models.experimental import attempt_load


class ModelWrapper(torch.nn.Module):
    def __init__(self, model):
        super().__init__()
        self.model = model

    def forward(self, *args, **kwargs):
        # return only the predictions as in the original model
        return self.model(*args, **kwargs)[0]


def load_model(pretrained: bool = False) -> torch.nn.Module:
    model = torch.hub.load("ultralytics/yolov5", "yolov5s", verbose=False, device="cpu")

    if pretrained:
        model.names = {0: "wheat head"}
        model.model = ModelWrapper(attempt_load("wheat_head_detection_model.pt"))

    return model
